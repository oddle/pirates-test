const addHook = require('pirates').addHook

addHook(
  (code, filename) => console.log('hook', filename) || code, 
  { exts: ['.js'], matcher: filename => filename.endsWith('es6.js') }
)
